import { createStore } from 'redux'

import { rootReducer } from "./reducers"

import { initialState as initialStateName } from "./state-names"


// const initialMessages = [{username: "test", timestamp: -1, content: "This is a test"}] // comment out in production
const initialMessages = [] // use this in production

const initialState = {state:  initialStateName, isVideoAvail: false, isAudioAvail: false, knownPeers: -1, messages: initialMessages}
// message => {username, timestamp, content}

const store = createStore(rootReducer, initialState)

export default store
