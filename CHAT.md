Assumptions
- Chat state doesn't need to be peristed between refresh
- No concepts chatrooms, whoever is in the RTCConnection
- No encryption beyond defaults
- No delivery receipts

## Connection
[x] negotiate new RTCDataChannel as part of initial handshake
[ ] link the data channel to UI inputs (finding the right place for the hook)


## Client message state
[x] Define a message: {username, timestamp, content}
[x] Setup reducers for incoming and outgoing messages. Probably add to state: {messages: []}


## UI
[x] The actual HTML structure
[ ] Handling user input
[ ] Displaying new messages